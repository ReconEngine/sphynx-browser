



from PyQt5.QtGui import *
from tkinter import *
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QPushButton, QMessageBox, QAction
from PyQt5 import QtGui
from PyQt5.QtWidgets import QMenu
import os
import subprocess
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWebEngineWidgets import *
from PyQt5.QtPrintSupport import *
import sys




class AboutDialog(QDialog):
    def __init__(self, *args, **kwargs):
        super(AboutDialog, self).__init__(*args, **kwargs)
        

        QBtn = QDialogButtonBox.Ok  # No cancel
        self.buttonBox = QDialogButtonBox(QBtn)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)


        layout = QVBoxLayout()

        title = QLabel("𝗛𝗘𝗟𝗣 𝗠𝗘𝗡𝗨")
        font = title.font()
        font.setPointSize(20)
        title.setFont(font)

        layout.addWidget(title)

        logo = QLabel()
        logo.setPixmap(QPixmap(os.path.join('images', 'ma-icon-128.png')))
        layout.addWidget(logo)

        layout.addWidget(QLabel("""With Sphynx Browser you can
proudly brows the web anonymously.
Save your data and much more!
Please report any bugs
to the dev team."""))
        layout.addWidget(QLabel("""
𝙲𝙾𝙽𝚃𝙰𝙲𝚃𝚂>
ɪɴꜱᴛᴀɢʀᴀᴍ: @idice.security"""))

        

        for i in range(0, layout.count()):
            layout.itemAt(i).setAlignment(Qt.AlignHCenter)

        layout.addWidget(self.buttonBox)

        self.setLayout(layout)



        
        


# Initialising and creating the main window

class MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)


        # Tab variables
        

        self.tabs = QTabWidget()
        self.tabs.setDocumentMode(True)
        self.tabs.tabBarDoubleClicked.connect(self.tab_open_doubleclick)
        self.tabs.currentChanged.connect(self.current_tab_changed)
        self.tabs.setTabsClosable(True)
        self.tabs.tabCloseRequested.connect(self.close_current_tab)
        self.setCentralWidget(self.tabs)
  
        
        self.resize(400, 200)



        # Just makes the window maximised

        self.showMaximized()

        # Navigation bar variables

        self.status = QStatusBar()
        self.setStatusBar(self.status)
        navtb = QToolBar("Navigation")
        self.addToolBar(navtb)
        
        
        
        
        
        
        

        home_btn = QAction('⌂', self)
        home_btn.setStatusTip("Go home")
        home_btn.triggered.connect(self.navigate_home)
        navtb.addAction(home_btn)

        # Button variables and functions
        back_btn = QAction('↩', self)
        back_btn.setStatusTip("Back to previous page")
        back_btn.triggered.connect(lambda: self.tabs.currentWidget().back())
        navtb.addAction(back_btn)

        forward_btn = QAction('↪', self)
        forward_btn.setStatusTip("Forward to next page")
        forward_btn.triggered.connect(lambda: self.tabs.currentWidget().forward())
        navtb.addAction(forward_btn)

        reload_btn = QAction('⟳', self)
        reload_btn.setStatusTip("Reload page")
        reload_btn.triggered.connect(lambda: self.tabs.currentWidget().reload())
        navtb.addAction(reload_btn)

        # search button
        search_btn = QAction("🌐", self)
        search_btn.setStatusTip("Search")
        search_btn.triggered.connect(self.navigate_search)
        navtb.addAction(search_btn)
        
        
        
        options = self.menuBar().addMenu("&Menu")
        
        peoplesearch = QAction(QIcon(os.path.join('images', ' ')), "People Search...", self)
        peoplesearch.setStatusTip("Find information about yourself or family members")
        peoplesearch.triggered.connect(self.peoplesearch)
        options.addAction(peoplesearch)
        
        proton = QAction(QIcon(os.path.join('images', ' ')), "VPN Service...", self)
        proton.setStatusTip("Protect yourself online by downloading protonvpn, a secure private vpn service!")
        proton.triggered.connect(self.proton)
        options.addAction(proton)
        
        save_file_action = QAction(QIcon(os.path.join('images', ' ')), "Save Page...", self)
        save_file_action.setStatusTip("Save current page to file")
        save_file_action.triggered.connect(self.save_file)
        options.addAction(save_file_action)
        
        pirate = QAction(QIcon(os.path.join('images', ' ')), "PirateBay...", self)
        pirate.setStatusTip("Gain access to paid services for free")
        pirate.triggered.connect(self.piratebay)
        options.addAction(pirate)
        
        open_file_action = QAction(QIcon(os.path.join('images', ' ')), "Open file...", self)
        open_file_action.setStatusTip("Open from file")
        open_file_action.triggered.connect(self.open_file)
        options.addAction(open_file_action)
        
        viewdns = QAction(QIcon(os.path.join('images', ' ')), "ViewDns...", self)
        viewdns.setStatusTip("View the contents of a website")
        viewdns.triggered.connect(self.viewdns)
        options.addAction(viewdns)
        
        
        storage = self.menuBar().addMenu("&Storage")

        
        
        haste = QAction(QIcon(os.path.join('images', ' ')), "HasteBin...", self)
        haste.setStatusTip("Save your data on hastebin")
        haste.triggered.connect(self.haste)
        storage.addAction(haste)

        pastebin = QAction(QIcon(os.path.join('images', ' ')), "PasteBin...", self)
        pastebin.setStatusTip("Save your data on pastebin")
        pastebin.triggered.connect(self.pastebin)
        storage.addAction(pastebin)
        
        control = QAction(QIcon(os.path.join('images', ' ')), "Controlc...", self)
        control.setStatusTip("Save your data on controlc")
        control.triggered.connect(self.control)
        storage.addAction(control)

        fiddle = QAction(QIcon(os.path.join('images', ' ')), "JsFiddle...", self)
        fiddle.setStatusTip("Save your data on jsfiddle")
        fiddle.triggered.connect(self.fiddle)
        storage.addAction(fiddle)
        
        zerobin = QAction(QIcon(os.path.join('images', ' ')), "ZeroBin...", self)
        zerobin.setStatusTip("Save your data on zerobin")
        zerobin.triggered.connect(self.zerobin)
        storage.addAction(zerobin)
        
        skidbin = QAction(QIcon(os.path.join('images', ' ')), "SkidBin...", self)
        skidbin.setStatusTip("Save your data on skidbin")
        skidbin.triggered.connect(self.skidbin)
        storage.addAction(skidbin)

        dpaste = QAction(QIcon(os.path.join('images', ' ')), "DPaste...", self)
        dpaste.setStatusTip("Save your data on dpaste")
        dpaste.triggered.connect(self.dpaste)
        storage.addAction(dpaste)

        paste = QAction(QIcon(os.path.join('images', ' ')), "Paste...", self)
        paste.setStatusTip("Save your data on paste")
        paste.triggered.connect(self.paste)
        storage.addAction(paste)


        #file_menu = self.menuBar().addMenu("&File")

        #open_file_action = QAction(QIcon(os.path.join('images', ' ')), "Open file...", self)
        #open_file_action.setStatusTip("Open from file")
        #open_file_action.triggered.connect(self.open_file)
        #file_menu.addAction(open_file_action)
        
        
        help_menu = self.menuBar().addMenu("&Help")

        about_action = QAction(QIcon(os.path.join('images', '?')), "Help Menu", self)
        about_action.setStatusTip("Find out more about the Browser")  # Hungry!
        about_action.triggered.connect(self.about)
        help_menu.addAction(about_action)

        navigate_mozarella_action = QAction(QIcon(os.path.join('images', ' ')),
                                            "Help", self)
        navigate_updates_action = QAction(QIcon(os.path.join('images', ' ')),
                                           "Updates", self)
        
        discord = QAction(QIcon(os.path.join('images', ' ')), "Discord...", self)
        discord.setStatusTip("Join our discord server!")
        discord.triggered.connect(self.discord)
        help_menu.addAction(discord)
        
        

        

        navtb.addSeparator()
        
        

        # Adding URL bar

        self.urlBar = QLineEdit()
        self.urlBar.returnPressed.connect(self.navigate_to_url)
        navtb.addWidget(self.urlBar)

        # New tab

        tab_btn = QAction('⏏', self)
        tab_btn.setStatusTip("Open new tab")
        tab_btn.triggered.connect(self.navigate_tab)
        navtb.addAction(tab_btn)

        # Stop loading

        stop_btn = QAction('✘', self)
        stop_btn.setStatusTip("Stop loading current page")
        stop_btn.triggered.connect(lambda: self.tabs.currentWidget().stop())
        navtb.addAction(stop_btn)

        # Start screen on launch

        self.add_new_tab(QUrl('http://sphynx-browser.website2.me/'), 'homepage')
        self.show()
        self.setWindowTitle('𝓢𝓹𝓱𝔂𝓷𝔁  𝓑𝓻𝓸𝔀𝓼𝓮𝓻')



        # menu loading

        #menu_btn = QAction('≣', self)
        #menu_btn.setStatusTip("Open menu options")
        #menu_btn.triggered.connect(lambda: self.createMenuBar())
        #navtb.addAction(menu_btn)

    def about(self):
        dlg = AboutDialog()
        dlg.exec_()


        
        

    # Adding new tabs
    
    def open_file(self):
        filename, _ = QFileDialog.getOpenFileName(self, "Open file", "",
                                                  "Hypertext Markup Language (*.htm *.html);;"
                                                  "All files (*.*)")

        if filename:
            with open(filename, 'r') as f:
                html = f.read()

            self.tabs.currentWidget().setHtml(html)
            self.urlbar.setText(filename)

    def save_file(self):
        filename, _ = QFileDialog.getSaveFileName(self, "Save Page As", "",
                                                  "Hypertext Markup Language (*.htm *html);;"
                                                  "All files (*.*)")

        if filename:
            html = self.tabs.currentWidget().page().toHtml()
            with open(filename, 'w') as f:
                f.write(html.encode('utf8'))

    

    def add_new_tab(self, qurl=None, label="Loading..."):
        if qurl is None:
            qurl = QUrl('https://duckduckgo.com')

        browser = QWebEngineView()
        browser.setUrl(qurl)

        i = self.tabs.addTab(browser, label)
        self.tabs.setCurrentIndex(i)

        browser.urlChanged.connect(lambda qurl, browser=browser: self.update_urlbar(qurl, browser))

        browser.loadFinished.connect(lambda _, i=i, browser=browser: self.tabs.setTabText(i, browser.page().title()))

    # Making tab open on double click

    def tab_open_doubleclick(self, i):

        if i == -1:
            self.add_new_tab()

    def current_tab_changed(self, i):
        qurl = self.tabs.currentWidget().url()
        self.update_urlbar(qurl, self.tabs.currentWidget())
        self.update_title(self.tabs.currentWidget())

    def close_current_tab(self, i):
        if self.tabs.count() < 2:
            return

        self.tabs.removeTab(i)

    def update_title(self, browser):
        if browser != self.tabs.currentWidget():
            return

        title = self.tabs.currentWidget().page().title()
        self.setWindowTitle("% s - 𝓢𝓹𝓱𝔂𝓷𝔁  𝓑𝓻𝓸𝔀𝓼𝓮𝓻" % title)

    def navigate_home(self):
        self.tabs.currentWidget().setUrl(QUrl("http://sphynx-browser.website2.me/"))
        
        
    def piratebay(self):
        self.tabs.currentWidget().setUrl(QUrl("https://thepirate-bay.org/en/"))

    def viewdns(self):
        self.tabs.currentWidget().setUrl(QUrl("http://viewdns.info/"))
        
    def pastebin(self):
        self.tabs.currentWidget().setUrl(QUrl("http://pastebin.com/"))
        
    def zerobin(self):
        self.tabs.currentWidget().setUrl(QUrl("https://zerobin.net/"))

    def skidbin(self):
        self.tabs.currentWidget().setUrl(QUrl("https://skidbin.net/"))
        
    def dpaste(self):
        self.tabs.currentWidget().setUrl(QUrl("https://dpaste.com/"))
        
    def paste(self):
        self.tabs.currentWidget().setUrl(QUrl("https://paste.ee/"))

    def haste(self):
        self.tabs.currentWidget().setUrl(QUrl("https://hastebin.com/"))
        
    def fiddle(self):
        self.tabs.currentWidget().setUrl(QUrl("https://jsfiddle.net/")) 

    def control(self):
        self.tabs.currentWidget().setUrl(QUrl("http://controlc.com/"))
        
    def discord(self):
        self.tabs.currentWidget().setUrl(QUrl("https://discord.gg/5HF38v2AYd"))
        
    def peoplesearch(self):
        self.tabs.currentWidget().setUrl(QUrl("https://truepeoplesearch.com/"))

    def proton(self):
        self.tabs.currentWidget().setUrl(QUrl("https://protonvpn.com/"))

    def navigate_tab(self):
        self.add_new_tab()

    def navigate_search(self):
        self.tabs.currentWidget().setUrl(QUrl("https://duckduckgo.com/"))
        
        

        
        

    def navigate_to_url(self):
        q = QUrl(self.urlBar.text())
        if q.scheme() == "":
           
        
            q.setScheme("https")

        self.tabs.currentWidget().setUrl(q)

    def update_urlbar(self, q, browser=None):
        if browser != self.tabs.currentWidget():
            return

        self.urlBar.setText(q.toString())
        self.urlBar.setCursorPosition(0)
        
        


        



app = QApplication(sys.argv)

qp = QPalette()
qp.setColor(QPalette.ButtonText, Qt.cyan)
qp.setColor(QPalette.WindowText, Qt.cyan)
qp.setColor(QPalette.Window, Qt.black)
qp.setColor(QPalette.Button, Qt.black)
    
app.setPalette(qp)
app.setStyle("Fusion")


app.setApplicationName("𝓢𝓹𝓱𝔂𝓷𝔁  𝓑𝓻𝓸𝔀𝓼𝓮𝓻")
window = MainWindow()
app.setWindowIcon(QtGui.QIcon('icon_photo.png'))




app.exec_()





